package sample;

import java.io.File;
import java.util.Arrays;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		System.out.println("Введите адрес каталога для отображения вложенных в него каталогов: ");
		Scanner sc = new Scanner(System.in);
		String adress = sc.nextLine();
		
		browsingCatalog(adress);
	}
	
	private static void browsingCatalog(String adress) {
				
		File workFolder = new File(adress);
		File[] file = workFolder.listFiles();	
		for (int i = 0; i < file.length; i++) {
			if(file[i].isDirectory()) {
			System.out.println(file[i]);
			}
		}
	}
}

