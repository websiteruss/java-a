package sample;

import java.awt.Desktop;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

public class MapBuilding {
	private static Map<String, String> lingvo = new HashMap<>();

	public MapBuilding() {
		super();
		// TODO Auto-generated constructor stub
	}

	public MapBuilding(Map<String, String> lingvo) {
		super();
		this.lingvo = lingvo;
	}

	public void fileWorking(File file) throws FileNotFoundException {
		try (Scanner sc = new Scanner(file)) {
			String text = sc.nextLine();

			String[] arrText = text.toLowerCase().split("[ ]");

			File file1 = new File("Ukrainian.out");

			try (PrintWriter pw = new PrintWriter(file1)) {
				for (int i = 0; i < arrText.length; i++) {
					if (lingvo.get(arrText[i]) != null) {
						pw.print(lingvo.get(arrText[i]) + " ");
						System.out.print(lingvo.get(arrText[i]) + " ");
					} else {
						pw.print(arrText[i] + "(перевод отсутствует в словаре) ");
						System.out.println(arrText[i] + "(перевод отсутствует в словаре) ");
					}
				}
			}
		}
		System.out.println();
	}

	public void addToDictionary() throws FileNotFoundException {
		try (Scanner sc = new Scanner(System.in)) {
			System.out.println("Введите слово на английском: ");
			String engWord = sc.nextLine();
			System.out.println("Введите слово на украинском: ");
			String ukrWord = sc.nextLine();
			lingvo.put(engWord, ukrWord);
		}
		File file = new File("Dictionary.txt");
		try (PrintWriter pw = new PrintWriter(file)) {
			Set<Map.Entry<String, String>> hms = lingvo.entrySet();
			for (Map.Entry<String, String> hmse : hms) {
				pw.println(hmse.getKey() + ";" + hmse.getValue());

			}
		}
		System.out.println("Изменения внесены");
	}

	public void openDirectory() {
		File file = new File(".");

		Desktop desktop = null;

		if (Desktop.isDesktopSupported()) {
			desktop = Desktop.getDesktop();
		}
		try {
			desktop.open(file);
		} catch (IOException e) {
			System.out.println(e);
		}
	}

	public Map<String, String> getLingvo() {
		return lingvo;
	}

	public void setLingvo(Map<String, String> lingvo) {
		this.lingvo = lingvo;
	}

	@Override
	public String toString() {
		return "MapBuilding [lingvo=" + lingvo + "]";
	}

}
