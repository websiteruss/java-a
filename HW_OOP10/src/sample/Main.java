package sample;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {

		Map<String, String> lingvo = null;
		try {
			lingvo = loadMapFromDictionary();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		MapBuilding map = new MapBuilding(lingvo);

		try {
			File file = new File("English.in");
			map.fileWorking(file);
		} catch (FileNotFoundException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}

		System.out.println("Хотите добавить в словарь новые значения? (Y/N): ");
		try (Scanner sc = new Scanner(System.in)) {
			String inputText = sc.nextLine();
			if (inputText.equals("Y") || inputText.equals("y")) {
				try {
					map.addToDictionary();
					map.openDirectory();
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				}
			} else {
				map.openDirectory();
			}
		}
		// System.out.println(map.getLingvo().get("example"));
		// System.out.println(map.getLingvo().get("skull"));

	}

	public static Map<String, String> loadMapFromDictionary() throws FileNotFoundException {
		Map<String, String> map = new HashMap<>();

		File file = new File("Dictionary.txt");

		try (Scanner sc = new Scanner(file)) {

			while (sc.hasNextLine()) {
				String text = sc.nextLine();
				String[] subStr = text.split(";");
				for (int i = 0; i < subStr.length; i++) {
					map.put(subStr[0], subStr[1]);
				}
			}
			return map;
		}

	}

}
