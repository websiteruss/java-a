package sample;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		tenList();
	}
		public static void tenList() {
			Scanner sc = new Scanner(System.in);

			List<String> myList = new ArrayList<>();
			System.out.println("Введите с клавиатуры первый элемент списка:");
			myList.add(sc.nextLine());
			System.out.println("Введите с клавиатуры второй элемент списка:");
			myList.add(sc.nextLine());
			System.out.println("Введите с клавиатуры третий элемент списка:");
			myList.add(sc.nextLine());
			System.out.println("Введите с клавиатуры четвёртый элемент списка:");
			myList.add(sc.nextLine());
			System.out.println("Введите с клавиатуры пятый элемент списка:");
			myList.add(sc.nextLine());
			System.out.println("Введите с клавиатуры шестой элемент списка:");
			myList.add(sc.nextLine());
			System.out.println("Введите с клавиатуры седьмой элемент списка:");
			myList.add(sc.nextLine());
			System.out.println("Введите с клавиатуры восьмой элемент списка:");
			myList.add(sc.nextLine());
			System.out.println("Введите с клавиатуры девятый элемент списка:");
			myList.add(sc.nextLine());
			System.out.println("Введите с клавиатуры десятый элемент списка:");
			myList.add(sc.nextLine());
			
			System.out.println(myList);
			
			System.out.println("Удалены 2 первых элемента из списка ");
			myList.remove(0);
			myList.remove(0);
			System.out.println(myList);
			
			System.out.println("Удалён последний элемент из списка ");
			myList.remove(7);
			System.out.println(myList);

		}
		
}

