package com.example.hw_javapro1;

import java.io.*;

import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

@WebServlet(name = "helloServlet", value = "/hello-servlet")
public class HelloServlet extends HttpServlet {
    String textServlet = ("<html><body><h1>%s</h1>" +
            "<h2>Привет!</h2>" +
            "</body>" +
            "</html>");
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException{
        String FirstName = request.getParameter("FirstName");
        response.setContentType("text/html;charset=utf-8");
        response.getWriter().println(String.format(textServlet, FirstName));
    }

    public void destroy() {
    }
}