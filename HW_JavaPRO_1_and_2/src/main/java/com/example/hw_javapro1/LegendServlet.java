package com.example.hw_javapro1;

import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "LegendServlet", value = "/LegendServlet")
public class LegendServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=utf-8");
        HttpSession session = request.getSession();

        Legend legend = new Legend();

        legend.setJavaYes(request.getParameter("Yes1"));
        if (legend.getJavaYes() != null) {
            session.setAttribute("Yes1", legend);
        } else {
            session.setAttribute("No1", legend);
        }

        legend.setPythonYes(request.getParameter("Yes2"));
        if (legend.getPythonYes() != null) {
            session.setAttribute("Yes2", legend);
        } else {
            session.setAttribute("No2", legend);
        }

        legend.setJsYes(request.getParameter("Yes3"));
        if (legend.getJsYes() != null) {
            session.setAttribute("Yes3", legend);
        } else {
            session.setAttribute("No3", legend);
        }

        PrintWriter pw = response.getWriter();
        pw.println("<table border='1'>\n" +
                "<tr>\n" +
                "  <th>Question</th>\n" +
                "  <th>YES</th>\n" +
                "  <th>NO</th>\n" +
                "  </tr>\n" +
                " <tr>\n" +
                "  <td>Do you love Java?</td>\n");
        if (session.getAttribute("Yes1") != null) {
            pw.println("  <td>YES</td>\n" +
                    "  <td> </td>\n" +
                    " </tr>\n" +
                    "<tr>\n");
        } else {
            pw.println("  <td> </td>\n" +
                    "  <td>NO</td>\n" +
                    " </tr>\n" +
                    "<tr>\n");
        }
        pw.println("  <td>Do you love Python?</td>\n");
        if (session.getAttribute("Yes2") != null) {
            pw.println("  <td>YES</td>\n" +
                    "  <td> </td>\n" +
                    " </tr>\n" +
                    "<tr>\n");
        } else {
            pw.println("  <td> </td>\n" +
                    "  <td>NO</td>\n" +
                    " </tr>\n" +
                    "<tr>\n");
        }
        pw.println("  <td>Do you love JavaScript?</td>\n");
        if (session.getAttribute("Yes3") != null) {
            pw.println("  <td>YES</td>\n" +
                    "  <td> </td>\n" +
                    " </tr>\n" +
                    "<tr>\n");
        } else {
            pw.println("  <td> </td>\n" +
                    "  <td>NO</td>\n" +
                    " </tr>\n" +
                    "<tr>\n");
        }
        pw.println("</table>\n");
        pw.println("<p><a href=\"//localhost:8080/LegendServlet\" target=\"_blank\">Ссылка \n" +
                "  открывает новое окно для проверки сохранения данных</a></p>");
    }

}
