package com.example.hw_javapro1;

public class Legend {
    private String javaYes;
    private String pythonYes;
    private String jsYes;

    public Legend() {
    }

    public Legend(String javaYes, String pythonYes, String jsYes) {
        this.javaYes = javaYes;
        this.pythonYes = pythonYes;
        this.jsYes = jsYes;
    }

    public String getJavaYes() {
        return javaYes;
    }

    public void setJavaYes(String javaYes) {
        this.javaYes = javaYes;
    }

    public String getPythonYes() {
        return pythonYes;
    }

    public void setPythonYes(String pythonYes) {
        this.pythonYes = pythonYes;
    }

    public String getJsYes() {
        return jsYes;
    }

    public void setJsYes(String jsYes) {
        this.jsYes = jsYes;
    }
}
