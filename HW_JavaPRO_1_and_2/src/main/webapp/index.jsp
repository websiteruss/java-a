<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<body>
<div>
    <h2>Homework-1</h2>
    <i>Сделать простое приложение на сервлете, которое принимает запрос типа <br> /hello&name=xxx</i>
</div>
<p></p>
<div>
    <form action="/hello-servlet">
        Введите имя:
        <input type="text" name="FirstName"><br>
        Нажмите :
        <input type="submit" value="Протестировать запрос">
    </form>
</div>
<p></p>
*************************************************************************************************
<div>
    <h2>Homework-2</h2>
    <i>Сделать проект-анкету, который позволяет через форму ввести ответы на 2-3 вопроса.<br>
        Далее ответы сохраняються на бэкэнде и после заполнения анкеты пользователю <br>
        выводится таблица со всеми результатами заполнения.
    </i>
</div>
<p></p>
<form name=" " action="/LegendServlet" method="" id="">
    <fieldset>
        <legend>
            Анкета
        </legend>
        <p>1) Do you love Java?</p>
        <input type="radio" name="Yes1" value=""/> <small>Yes</small><br>
        <input type="radio" name="No1" value=""/> <small>No</small><br>
        <p>2) Do you love Python?</p>
        <input type="radio" name="Yes2" value=""/> <small>Yes</small><br>
        <input type="radio" name="No2" value=""/> <small>No</small><br>
        <p>3) Do you love JavaScript?</p>
        <input type="radio" name="Yes3" value=""/> <small>Yes</small><br>
        <input type="radio" name="No3" value=""/> <small>No</small><br>
        <p><input type="submit" value="Отправить"/>
    </fieldset>
</form>
</body>
</html>