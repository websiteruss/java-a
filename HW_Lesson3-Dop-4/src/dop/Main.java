package dop;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Дан треугольник с координатами вершин А(0,0), В(4,4), С(6,1)");
		System.out.println("Введите координату точки" + " x:");
		double x = sc.nextDouble();
		System.out.println("Введите координату точки" + " y:");
		double y = sc.nextDouble();
		
		double pspABO = 4 * (y - 0) - 4 * (x - 0);
		double pspBCO = 2 * (y - 4) - (-3) * (x - 4);
		double pspCAO = (-6) * (y - 1) - (-1) * (x - 6);
		
		if ((pspABO >= 0 && pspBCO >= 0 && pspCAO >= 0) || (pspABO <= 0 && pspBCO <= 0 && pspCAO <= 0)) {
			System.out.println("Точка "+"("+ x + ", " + y + ")"+ "принадлежит треугольнику ABC");
		}else {
			System.out.println("Координаты введенной точки не принадлежат треугольнику ABC ");
		}
	}
}
