package dop;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		System.out.println("Введите максимальную ширину (нечётное число) " +"песочных часов: ");
		Scanner sc = new Scanner(System.in);
		int b = sc.nextInt();
		hourglass(b);
	}

	private static void hourglass(int b) {
        for (int i = 1; i <= b; i++) {
            for (int j = 1; j <= b; j++) {
                if (i >= j && i >= (b + 1) - j || (i <= j && i <= (b + 1) - j)) {
                    System.out.print("*");
                } else {
                    System.out.print(" ");
                }
                if (j % b == 0) {
                    System.out.println("");
                }
            }
        }
	}
}
