package sample;

public class Dog extends Animal {

	private String name;
	private String voice;
	
	public Dog(String ration, String color, int weight, String name, String voice) {
		super(ration, color, weight);
		this.voice = voice;
		this.name = name;
	}

	public Dog() {
		
	}

	public String getVoice(String voice) {
		return voice;
	}
	public void eat() {
		System.out.println("Dog " + name + " is eating");
	}
	public void sleep() {
		System.out.println("Dog " + name + " is sleeping");
	}

	public String getVoice() {
		return voice;
	}

	public void setVoice(String voice) {
		this.voice = voice;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Dog [name=" + name + ", voice=" + voice + "]"+ super.toString();
	}
}
