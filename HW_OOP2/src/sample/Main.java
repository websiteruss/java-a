package sample;

public class Main {

	public static void main(String[] args) {

		Dog dog1 = new Dog("bones", "white", 5, "Pluto", "Gav-Gav");
		Dog dog2 = new Dog("Chappie", "black", 10,"Sharik", "Gav-Gavuuuuu");
		Cat cat1 = new Cat("fish-food", "tricolor", 4, "Murka", "Miyav-Miyavuuuuu");
		Cat cat2 = new Cat("cat-canned food", "gray", 5, "Tomas", "Miyav-Miyav");
		Rabbit rabbit = new Rabbit("grass", "white", 1, "Krol", "-");

		Veterinarian vet = new Veterinarian("Тарас Подоляк");
		vet.treatment(rabbit);
		vet.treatment(cat2);
		vet.treatment(cat1);
		vet.treatment(dog1);
		vet.treatment(dog2);

	}
}
