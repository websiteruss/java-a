package dop;

import java.util.Arrays;
import java.util.Scanner;

public class Main {
		static int endIndex;
		static int temp;
		
	public static void main(String[] args) {
		System.out.println("Выберите размерность двумерного массива: ");
		Scanner sc = new Scanner(System.in);
		int num = sc.nextInt();
		int[][] array = new int[num][num];
		
		endIndex = num - 1;
		
		System.out.println("Полученный массив: ");
		for (int i = 0; i < array.length; i++) {
			for (int j = 0; j < array[i].length; j++) {
				array[i][j] = (int) (Math.random() * 10);
			}
			System.out.println(Arrays.toString(array[i]));
		}
		System.out.println("");
		System.out.println("Массив, повернутый на 90°: ");
		for (int[] row : rotateAngle90(array)) {
			System.out.println(Arrays.toString(row));
			}			

		System.out.println("");
		System.out.println("Массив, повернутый на 180°: ");
		for (int[] row : rotateAngle90(array)) {
			System.out.println(Arrays.toString(row));
			}	

		System.out.println("");
		System.out.println("Массив, повернутый на 270°: ");
		for (int[] row : rotateAngle90(array)) {
			System.out.println(Arrays.toString(row));
			}
	}
	public static int[][] rotateAngle90(int[][] array) {
		for (int i = 0; i <= endIndex / 2; i++) {
			for (int j = 0 + i; j < endIndex - i; j++) {
				temp = array[i][j];
				array[i][j] = array[endIndex - j][i];
				array[endIndex - j][i] = array[endIndex - i][endIndex - j];
				array[endIndex - i][endIndex - j] = array[j][endIndex - i];
				array[j][endIndex - i] = temp;
			}
		}
		return array;
	}
}