package dop;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		System.out.println("Нахождение вещественного корня при помощи итерационной формулы Герона.");
		System.out.println("Введите число: ");
		Scanner sc = new Scanner(System.in);		
		double a = sc.nextDouble();
		System.out.println("Введите желаемую точность (~ 0,1): ");
		double b = sc.nextDouble();
		System.out.println("Вещественный корень числа: " + iteracioGeron(a, b));
	}

	public static double iteracioGeron(double a, double accuracy) {
		double x0 = a;
		double x1;
		double d;
		do {
			x1 = 0.5 * (x0 + a / x0 );
			d = Math.abs(x0 - x1 );
			x0 = x1;
		}while (d > accuracy * 2 || d * d > accuracy * 2);

		return x0;
	}
}
