package sample;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class ServiceCheck {

	public static Map<String, List<String>> getHeader(String spec) throws IOException{
		URL url = new URL(spec);
		URLConnection connection = url.openConnection();
		return connection.getHeaderFields();
	}

	public static int getRespCode(String spec) throws IOException {
		URL url = new URL(spec);
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		return ( connection.getResponseCode());
	}

	public static void websiteAccessibilityCheck(File file) throws IOException{

		String result = "";
		try (Scanner sc = new Scanner(file)) {
			while (sc.hasNextLine()) {
				result += sc.nextLine() + System.lineSeparator();
			}
		}
		String[] arrText = result.split("\n");

		for (int i = 0; i < arrText.length; i++) {
			if (!getHeader(arrText[i]).isEmpty() && getRespCode(arrText[i]) == 200) {
				System.out.println("Сайт " + arrText[i] + " доступен");
			}else {
				System.out.println("Сайт " + arrText[i] + " недоступен");
			}
		}
	}
}
