package sample;

import java.util.Arrays;

public class Main {

	public static void main(String[] args) {
		int[] array = new int[15];
		for (int i = 0; i < array.length; i ++) {
			int rand = (int) (Math.random()*(70+1)) - 35;
			array[i] = rand;
		}
		System.out.println("Было: " + Arrays.toString(array));
		
		int[] array2 = Arrays.copyOf(array, 30);
			int j = 0; 
			for (int i = (array2.length / 2); i < array2.length; i++) {
				array2[i] = array[j++] * 2;
		}
		System.out.println("Стало: " + Arrays.toString(array2));
	}

}
