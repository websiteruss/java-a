package sample;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Random rn = new Random();
		System.out.println("Ведите размер массива целых чисел: ");
		Scanner sc = new Scanner(System.in);
		int num = sc.nextInt();
		int[] array = new int[num];
		for(int i = 0; i < array.length; i++) {
			array[i] = rn.nextInt(-10, 50);
		}
		System.out.println(Arrays.toString(array));
		System.out.println("Максимальное число в массиве: " + findMax(array));
	}

	public static int findMax(int[] array) {
		int currentMax = array[0];
		for(int i = 0; i < array.length; i++) {
			if(array[i] > currentMax) {
				currentMax = array[i];
			}
		}
		return currentMax;
	}
}
