package dop;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		 Scanner sc = new Scanner(System.in);
		    int h;
		    System.out.println("Input h");
		    h = sc.nextInt();
		    figure(h);
	}

	private static void figure(int h) {
		int i = 1;
	    int j = 0;
	    for (; i <= 2 * h - 1;) {
	      System.out.print("*");
	      j = j + 1;
	      if (j == h - Math.abs(h - i)) {
	        System.out.println();
	        i = i + 1;
	        j = 0;
	      }
	    }
	}
}
