package sample;

import java.util.Arrays;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		System.out.println("Введите размер массива для его ручного заполнения: ");
		Scanner sc = new Scanner(System.in);
		int num = sc.nextInt();
		int[] array = new int[num];
		
		for(int i = 0; i < array.length; i++) {
			System.out.println("Введите элемент массива: ");
			array[i] = sc.nextInt();
		}
		System.out.println("Полученный массив: " + Arrays.toString(array));
	}
}
