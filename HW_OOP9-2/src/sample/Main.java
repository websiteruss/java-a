package sample;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {

		Group groupe = new Group();

		Student student1 = new Student("Семён", "Бузыцков", Gender.MALE, 1001, "JavaOOP");
		Student student2 = new Student("Геннадий", "Беленков", Gender.MALE, 1002, "JavaOOP");
		Student student3 = new Student("Елена", "Ижутина", Gender.FEMALE, 1003, "JavaOOP");
		Student student4 = new Student("Евгений", "Картавый", Gender.MALE, 1004, "JavaOOP");
		Student student5 = new Student("Анастасия", "Рыбюк", Gender.FEMALE, 1005, "JavaOOP");
		Student student6 = new Student("Рената", "Холопова", Gender.FEMALE, 1006, "JavaOOP");
		Student student7 = new Student("Александр", "Камышенко", Gender.MALE, 1007, "JavaOOP");
		Student student8 = new Student("Полина", "Румович", Gender.FEMALE, 1008, "JavaOOP");
		Student student9 = new Student("Геннадий", "Полежаев", Gender.MALE, 1009, "JavaOOP");
		Student student10 = new Student("Егор", "Кушнир", Gender.MALE, 1010, "JavaOOP");

		groupe.addStudent(student1);
		groupe.addStudent(student2);
		groupe.addStudent(student3);
		groupe.addStudent(student4);
		groupe.addStudent(student5);
		groupe.addStudent(student6);
		groupe.addStudent(student7);
		groupe.addStudent(student8);
		groupe.addStudent(student9);
		groupe.addStudent(student10);

		try {
			GroupFileStorage.saveGroupToCSV(groupe);
		} catch (IOException e) {
			e.printStackTrace();
		}

		File file = new File("JavaOOP.csv");
		try {
			Group groupeAfterload = GroupFileStorage.loadGroupFromCSV(file);
		} catch (IOException | GroupOverflowException e1) {
			e1.printStackTrace();
		}

		try {
			System.out.println(groupe.searchStudentByLastName("Ижутина"));
		} catch (StudentNotFoundException e) {
			e.printStackTrace();
		}

		groupe.removeStudentByID(1009);
		System.out.println();
		groupe.addStudent(student9);
		System.out.println();
		groupe.sortStudentsByLastName();
		System.out.println();
		groupe.checkStudentForUniqueness();

	}
}