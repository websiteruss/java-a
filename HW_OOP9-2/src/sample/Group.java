package sample;

import java.sql.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

public class Group implements Cloneable {
	private String groupName;
	private final List<Student> students;

	public Group(String groupName, List<Student> students) {
		super();
		this.groupName = groupName;
		this.students = new ArrayList<>(10);
	}

	public Group() {
		super();
		students = new ArrayList<>(10);
	}

	public void addStudent(Student student) {
		if (students.size() < 10) {
			students.add(student);
		}else {
			System.out.println("Количество студентов в группе должно быть не больше 10. Проверьте правильность заполнения группы.");
		}
		return;
	}

	public Student searchStudentByLastName(String lastName) throws StudentNotFoundException {

		for (Student element : students) {
			if (element.getLastName().equals(lastName)) {
				return element;
			}
		}
		throw new StudentNotFoundException();
	}

	public boolean removeStudentByID(int id) {
		for (Student element : students) {
			if (element.getId() == id) {
				students.remove(students.indexOf(element));
				return true;
			}
		}
		return false;
	}

	public void sortStudentsByLastName() {
		Collections.sort(students, Comparator.nullsFirst(new StudentComparator()));
		for (Student element : students) {
			System.out.println(element);
		}
	}

	public void checkStudentForUniqueness() {
		Object[] studentsForCheck = students.toArray();
		int s = 1;
		for (int j = 0; j < studentsForCheck.length; j++) {
			for (int i = 0; i < studentsForCheck.length; i++) {
				s += 1;
				if (j == i)
					continue;
				if (studentsForCheck[j].equals(studentsForCheck[i])) {
					System.out.println("Проверьте правильность заполнения группы. Повторяющиеся студенты");
					return;
				} else if (s == (studentsForCheck.length * studentsForCheck.length)) {
					System.out.println("Все студенты группы уникальны");
				}
			}
		}
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public List<Student> getStudents() {
		return students;
	}

	@Override
	public String toString() {
		return "Group [groupName=" + groupName + ", students=" + students + "]";
	}
	/*
	 * @Override public int hashCode() { final int prime = 31; int result = 1;
	 * result = prime * result + Arrays.hashCode(students); result = prime * result
	 * + Objects.hash(groupName); return result; }
	 * 
	 * @Override public boolean equals(Object obj) { if (this == obj) return true;
	 * if (obj == null) return false; if (getClass() != obj.getClass()) return
	 * false; Group other = (Group) obj; return Objects.equals(groupName,
	 * other.groupName) && Arrays.equals(students, other.students); }
	 * 
	 * @Override protected Group clone() throws CloneNotSupportedException { // TODO
	 * Auto-generated method stub return (Group) super.clone(); }
	 */
}
