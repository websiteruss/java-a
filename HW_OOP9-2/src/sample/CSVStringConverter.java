package sample;

public class CSVStringConverter implements StringConverter {
	@Override
	public String toStringRepresentation(Student student) {

		String str = student.getName() + ", " + student.getLastName() + ", " + student.getGender() + ", "
				+ student.getId() + ", " + student.getGroupName();

		return str;
	}

	@Override
	public Student fromStringRepresentation(String str) {

		String[] arr = str.split(", ");
		Student student = new Student(arr[0], arr[1], setGenderFunc(arr[2]), Integer.parseInt(arr[3]), arr[4]);

		return student;
	}

	public Gender setGenderFunc(String gender) {
		Gender gdr = Gender.FEMALE;
		if (gender.equals("MALE")) {
			gdr = Gender.MALE;
		}
		return gdr;
	}

}
