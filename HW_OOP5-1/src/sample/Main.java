package sample;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Введите абсолютный адрес каталога, из которого производится копирование файлов: ");
		String folder1 = sc.nextLine();
		System.out.println("Введите абсолютный адрес каталога для копирования: ");
		String folder2 = sc.nextLine();
		System.out.println("Введите разрешение нужных для копирования файлов: ");
		String fileXtension = sc.nextLine();

        File folderIn = new File(folder1);
        File folderOut = new File(folder2);
        
        try {
			FileSvce.copyAllFiles(folderIn, folderOut, fileXtension);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
