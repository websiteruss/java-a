package sample;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class FileSvce {

	public static long copyFile(File fileIn, File fileOut) throws IOException {
		try (InputStream is = new FileInputStream(fileIn); OutputStream os = new FileOutputStream(fileOut)) {
			return is.transferTo(os);
		}
	}

	public static int dot(String text) {
		char[] g = text.toCharArray();
		int h = 0;
		for (int i = 0; i < g.length; i++) {
			if (g[i] == '.') {
				h = i;
			}
		}
		return h;
	}

	public static void copyAllFiles(File folderIn, File folderOut, String fileXtension) throws IOException {
		File[] files = folderIn.listFiles();
		for (int i = 0; i < files.length; i++) {
			if (files[i].isFile()
					&& ((files[i].getName().substring((dot(files[i].getName()) + 1), files[i].getName().length())))
							.equals(fileXtension)) {
				File fileOut = new File(folderOut, files[i].getName());
				System.out.println(copyFile(files[i], fileOut));
				System.out.println(files[i].getName());
			}
		}
	}
}