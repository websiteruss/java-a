package sample;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		/*Один литр топлива стоит 1.2$. Ваш автомобиль тратит на 100 км пути 8 литров топлива.
		Вы собрались в поездку в соседний город. Расстояние до этого города составляет 120
		км. Вычислите и выведите на экран сколько вам нужно заплатить за топливо для
		поездки.*/
		
		Scanner sc = new Scanner(System.in);
		double patrolPrice = 1.2; 
		System.out.println("Стоимость литра топлива: $" + patrolPrice);
		
		System.out.println("Введите расход топлива Вашего автомобиля на 100км: л.");
		double consumption = sc.nextDouble();

		System.out.println("Введите расстояние для расчёта стоимости поездки: км.");
		double distance = sc.nextDouble();
		double patrolCost = (consumption * patrolPrice * distance) / 100;
		
		System.out.println("Стоимость Вашей поездки: $" + patrolCost);
	}
}
