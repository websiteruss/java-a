package sample;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		squareDrawing();
	}

	public static void squareDrawing() {
	    Scanner sc = new Scanner(System.in);
	    System.out.println("Введите высоту прямоугольника: ");
	    int h = sc.nextInt();
	    System.out.println("Введите ширину прямоугольника: ");
	    int w = sc.nextInt();
	    
		for (int i = 1; i <= h; i++) {
			for (int j = 1; j <= w; j++) {
				if ((i > 1) && (j > 1) && (i < h) && (j < w)) {
					System.out.print(" ");
				} else {
					System.out.print("*");
				} 
			}
			System.out.println();
		}
	  }	
}

