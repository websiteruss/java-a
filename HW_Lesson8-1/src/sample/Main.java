package sample;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;


public class Main {

	public static void main(String[] args) {
		
		System.out.println("Введите текст в файл 'yourText.txt': ");
		File file = new File("yourText.txt");
		Scanner sc = new Scanner(System.in);

		try (PrintWriter pw = new PrintWriter(file)) {
			pw.println(sc.nextLine());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}


