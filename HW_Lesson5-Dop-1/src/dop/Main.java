package dop;

import java.util.Arrays;
import java.util.Scanner;

public class Main {
	static int num;

	public static void main(String[] args) {
		
		System.out.println("Введите размер массива для его ручного заполнения: ");
		Scanner sc = new Scanner(System.in);
		num = sc.nextInt();
		int[] array = newArray(num, 0);
		System.out.println(Arrays.toString(array));
		arrayMirrow(array);
		System.out.println("Зеркальный переворот массива: " + Arrays.toString(array));

	}

	public static int[] newArray(int num, int i) {

		int[] array = new int[num];
		for(; i < array.length; i++) {
			System.out.println("Введите элемент массива: ");
			Scanner sc = new Scanner(System.in);
			array[i] = sc.nextInt();
		}
			return array;
	}
	
	public static void arrayMirrow(int[] array) {
		
		int startIndex = 0;
		int endIndex = array.length - 1;
		int middleIndex = (startIndex + endIndex) / 2;
		for(int i = startIndex; i <= middleIndex; i++) {
			int temp = array[i];
			array[i] = array[endIndex + startIndex - i];
			array[endIndex + startIndex - i] = temp;
		}
	}
}

	

