package sample;

public class Main {

	public static void main(String[] args) {
		/*Стоимость яблока составляет 2$. Покупатель приобретает 6 яблок. Напишите
		программу которая вычислит и выведет на экран сумму которую должен уплатить
		покупатель за покупку.*/
		
		double appleCost = 2.0;
		int apple = 6;
		
		double totalSum = appleCost * apple;
		System.out.println("Сумма покупки: $"+totalSum);
	}
}
