package sample;

public class Main {

	public static void main(String[] args) {
		
		Faucet franke1 = new Faucet ("Franke", "Sirius", "Хром", 3860);
		
		System.out.println("Бренд: " + franke1.getBrand());
		System.out.println("Модель: " + franke1.getModel());
		System.out.println("Цвет: " + franke1.getColor());
		System.out.println("Цена: " + franke1.getPrice() + " грн.");
		
		Faucet grohe1 = new Faucet();
		
		grohe1.setBrand("Grohe");
		grohe1.setModel("Blue Pure Mono");
		grohe1.setColor("Хром");
		grohe1.setPrice(6120);
		
		System.out.println();
		
		System.out.println("Бренд: " + grohe1.getBrand());
		System.out.println("Модель: " + grohe1.getModel());
		System.out.println("Цвет: " + grohe1.getColor());
		System.out.println("Цена: " + grohe1.getPrice() + " грн.");
		
	}

}
