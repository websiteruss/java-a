package sample;

public class Main {

	public static void main(String[] args) {
		StackClass stack = new StackClass(3);
		stack.push("Stack");
		stack.push("Java");
		stack.push("Hello");

		System.out.println(stack);
		System.out.println(stack.peek());
		System.out.println();

		for (; stack.peek() != null;) {
			System.out.println(stack.pop());
		}

	}

}
