package sample;

import java.util.Arrays;

public class StackClass {
	private Object[] dataArray;
	private int index;
	private int capacity;
	private final int MAX_SIZE = Integer.MAX_VALUE - 1;

	public StackClass(int x) {
		dataArray = new Object[x];
		capacity = dataArray.length;
		index = 0;
	}

	public void push(Object obj) {
		if (index >= capacity) {
			boolean resize = upResize();
			if (!resize) {
				throw new RuntimeException("Cannot add element");
			}
		}
		dataArray[index] = obj;
		index += 1;
	}

	public Object pop() {
		if (index == 0) {
			return null;
		}
		index -= 1;
		Object element = dataArray[index];
		dataArray[index] = null;
		return element;
	}

	public Object peek() {
		if (index == 0) {
			return null;
		}
		return dataArray[index - 1];
	}

	public boolean upResize() {
		if (capacity >= MAX_SIZE) {
			return false;
		}
		long newCapacityL = (capacity * 3L) / 2L + 1L;
		int newCapacity = (newCapacityL < MAX_SIZE ? (int) newCapacityL : MAX_SIZE);
		dataArray = Arrays.copyOf(dataArray, newCapacity);
		capacity = newCapacity;
		return true;

	}

}
