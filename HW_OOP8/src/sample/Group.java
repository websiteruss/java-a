package sample;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Objects;

public class Group implements Cloneable {
	private String groupName;
	private final Student[] students;

	public Group(String groupName, Student[] students) {
		super();
		this.groupName = groupName;
		this.students = new Student[10];
	}

	public Group() {
		super();
		students = new Student[10];
	}

	public void addStudent(Student student) throws GroupOverflowException {
		for (int i = 0; i < students.length; i++) {
			if (students[i] == null) {
				students[i] = student;
				return;
			}
		}
		throw new GroupOverflowException();
	}

	public Student searchStudentByLastName(String lastName) throws StudentNotFoundException {
		for (int i = 0; i < students.length; i++) {
			if (students[i] != null) {
				if (students[i].getLastName().equals(lastName)) {
					return students[i];
				}
			}
		}
		throw new StudentNotFoundException();
	}

	public boolean removeStudentByID(int id) {
		for (int i = 0; i < students.length; i++) {
			if (students[i] != null) {
				if (students[i].getId() == id) {
					students[i] = null;
					return true;
				}
			}
		}
		return false;
	}

	public void sortStudentsByLastName() {
		Arrays.sort(students, Comparator.nullsFirst(new StudentComparator()));
		for (int i = 0; i < students.length; i++) {
			System.out.println(students[i]);
		}
	}

	public void CheckStudentForUniqueness() {
		int s = 1;
		for (int j = 0; j < students.length; j++) {
			for (int i = 0; i < students.length; i++) {
				s += 1;
				if (j == i)
					continue;
				if (students[j].equals(students[i])) {
					System.out.println("Проверьте правильность заполнения группы. Повторяющиеся студенты");
					return;
				} else if (s == (students.length * students.length)) {
					System.out.println("Все студенты группы уникальны");
				}
			}
		}
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public Student[] getStudents() {
		return students;
	}

	@Override
	public String toString() {
		return "Group [groupName=" + groupName + ", students=" + Arrays.toString(students) + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(students);
		result = prime * result + Objects.hash(groupName);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Group other = (Group) obj;
		return Objects.equals(groupName, other.groupName) && Arrays.equals(students, other.students);
	}

	@Override
	protected Group clone() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		return (Group) super.clone();
	}

}
