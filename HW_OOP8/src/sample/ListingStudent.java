package sample;

import java.util.Scanner;

public class ListingStudent extends Student {

	public ListingStudent() {
		super();
	}

	public ListingStudent(String name, String lastName, Gender gender, int id, String groupName) {
		super(name, lastName, gender, id, groupName);
	}

	public Student listingStudent() {
		Scanner sc = new Scanner(System.in);

		System.out.println("Введите имя студента: ");
		String name = sc.nextLine();

		System.out.println("Введите фамилию студента: ");
		String lastName = sc.nextLine();

		System.out.println("Название группы: ");
		String groupName = sc.nextLine();

		System.out.println("Введите пол в формате 'MALE, FEMALE' : ");
		String gender = sc.nextLine();

		System.out.println("Введите номер зачётки студента: ");
		int id = sc.nextInt();

		Student student = new Student();

		student.setName(name);
		student.setLastName(lastName);
		student.setGroupName(groupName);
		student.setId(id);

		if (gender.equals("MALE")) {
			student.setGender(Gender.MALE);
		} else if (gender.equals("FEMALE")) {
			student.setGender(Gender.FEMALE);
		} else {
			System.out.println(
					"Некорректный ввод графы 'пол'. Перезапустите программу и введите пол в формате 'MALE, FEMALE' !");
		}

		return student;
	}

}
