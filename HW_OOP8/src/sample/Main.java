package sample;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {

		/*
		 * ListingStudent list = new ListingStudent(); Group groupe = new Group();
		 * 
		 * for (int i = 0; i < groupe.getStudents().length; i++) { try {
		 * groupe.addStudent(list.listingStudent()); } catch (GroupOverflowException e)
		 * { // TODO Auto-generated catch block e.printStackTrace(); } }
		 * groupe.sortStudentsByLastName();
		 */
		Group groupe = new Group();
		Student student1 = new Student("Семён", "Бузыцков", Gender.MALE, 1001, "JavaOOP");
		Student student2 = new Student("Геннадий", "Беленков", Gender.MALE, 1002, "JavaOOP");
		Student student3 = new Student("Елена", "Ижутина", Gender.FEMALE, 1003, "JavaOOP");
		Student student4 = new Student("Евгений", "Картавый", Gender.MALE, 1004, "JavaOOP");
		Student student5 = new Student("Анастасия", "Рыбюк", Gender.FEMALE, 1005, "JavaOOP");
		Student student6 = new Student("Рената", "Холопова", Gender.FEMALE, 1006, "JavaOOP");
		Student student7 = new Student("Александр", "Камышенко", Gender.MALE, 1007, "JavaOOP");
		Student student8 = new Student("Полина", "Румович", Gender.FEMALE, 1008, "JavaOOP");
		Student student9 = new Student("Геннадий", "Полежаев", Gender.MALE, 1009, "JavaOOP");
		// Student student10 = new Student("Егор", "Кушнир", Gender.MALE, 1010,
		// "JavaOOP");
		Student student10 = new Student("Геннадий", "Полежаев", Gender.MALE, 1009, "JavaOOP");

		try {
			groupe.addStudent(student1);
			groupe.addStudent(student2);
			groupe.addStudent(student3);
			groupe.addStudent(student4);
			groupe.addStudent(student5);
			groupe.addStudent(student6);
			groupe.addStudent(student7);
			groupe.addStudent(student8);
			groupe.addStudent(student9);
			groupe.addStudent(student10);
		} catch (GroupOverflowException e) {
			e.printStackTrace();
		}

		/*
		 * System.out.println(groupe);
		 * 
		 * 
		 * try { Student lastName = groupe.searchStudentByLastName("Ижутина");
		 * System.out.println(lastName); System.out.println(); } catch
		 * (StudentNotFoundException e) { e.printStackTrace(); }
		 * 
		 * System.out.println(groupe.removeStudentByID(1005));
		 * 
		 * groupe.sortStudentsByLastName();
		 */

		try {
			GroupFileStorage.saveGroupToCSV(groupe);
		} catch (IOException e) {
			e.printStackTrace();
		}

		File file = new File("JavaOOP.csv");
		try {
			Group groupeAfterload = GroupFileStorage.loadGroupFromCSV(file);
			groupeAfterload.sortStudentsByLastName();
		} catch (IOException | GroupOverflowException e1) {
			e1.printStackTrace();
		}
		System.out.println("-------------------------------");
		/*
		 * Scanner sc = new Scanner(System.in);
		 * System.out.println("Введите адрес рабочего каталога: "); File workFolder =
		 * new File(sc.nextLine());
		 * 
		 * System.out.println("Введите название группы: "); String searchedFile =
		 * sc.nextLine(); String groupName = searchedFile + ".csv";
		 * 
		 * try { File f = GroupFileStorage.findFileByGroupName(groupName, workFolder);
		 * if (searchedFile.equals(student1.getGroupName())) {
		 * System.out.println("Файл группы создан: " + f.getAbsolutePath()); } else {
		 * System.out.println("Файла группы с таким названием нет в каталоге"); } }
		 * catch (IOException e) { e.printStackTrace(); }
		 */
		Student studentClone = new Student();
		try {
			studentClone = student1.clone();
			// System.out.println(studentClone);
			System.out.println(student1.equals(studentClone));
			System.out.println("HashCode studentClone: " + studentClone.hashCode() + "  " + "HashCode student1: "
					+ student1.hashCode());
		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Group groupe1 = new Group();
		try {
			groupe1 = groupe.clone();
			// System.out.println(groupe1);
			System.out.println(groupe1.equals(groupe));
			System.out.println(
					"HashCode groupe: " + groupe.hashCode() + "  " + "HashCode groupe1: " + groupe1.hashCode());
		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("-------------------------------");
		groupe.CheckStudentForUniqueness();
	}
}