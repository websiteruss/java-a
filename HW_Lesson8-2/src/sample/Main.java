package sample;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		System.out.println("Выберите размерность двумерного массива: ");
		Scanner sc = new Scanner(System.in);
		int num = sc.nextInt();
		int[][] array = new int[num][num];

		for (int i = 0; i < array.length; i++) {
			for (int j = 0; j < array[i].length; j++) {
				array[i][j] = (int) (Math.random() * 10);
			}		
			}
		for (int[] row : array) {
			System.out.println(Arrays.toString(row));
		}
		printingArray(array);
	    
	}

	private static void printingArray(int[][] array) {
		
		File fileArray = new File("ArrayToFile.txt");
		
			try (PrintWriter pw = new PrintWriter(fileArray)){
				for (int[] row : array) {
					pw.println(Arrays.toString(row));
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
	}
}
