package com.example.hw_javapro_3;

import java.io.FileNotFoundException;
import java.lang.reflect.Method;
import java.net.StandardSocketOptions;

public class Reflection {
    public static void main(String[] args) {
        final Class<?> cls = TestClass.class;
        final Class<?> cls1 = TextContainer.class;

        Method[] methods = cls.getDeclaredMethods();
        Method[] methods1 = cls1.getDeclaredMethods();

        for (Method method : methods) {
            if (method.isAnnotationPresent(Test.class)) {
                Test anParam = method.getAnnotation(Test.class);
                TestClass.test(anParam.a(), anParam.b());
            }
        }
        if (cls1.isAnnotationPresent(SaveTo.class)) {
            for (Method method1 : methods1) {
                if (method1.isAnnotationPresent(Saver.class)) {
                    SaveTo anParam = cls1.getAnnotation(SaveTo.class);
                    System.out.println();
                    System.out.println("Проверьте в откывшемся окне файл 'file.txt' для проверки реализации домашнего задания 3.2");
                    try {
                        TextContainer.save(anParam.path());
                    } catch (FileNotFoundException e) {
                        throw new RuntimeException(e);
                    }
                    TextContainer.openDirectory();
                }
            }
        }
    }
}
