package com.example.hw_javapro_3;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.awt.Desktop;

@SaveTo
public class TextContainer {
    static String text = "Проверочная строка для сохранения в файл, указанный в параметре аннотации SaveTo";

    @Saver
    public static void save(String path) throws FileNotFoundException {
        File file = new File(path);
        try (PrintWriter pw = new PrintWriter(file)) {
            pw.println(text);
        }

    }

    public static void openDirectory() {
        File file = new File(".");

        Desktop desktop = null;

        if (Desktop.isDesktopSupported()) {
            desktop = Desktop.getDesktop();
        }
        try {
            desktop.open(file);
        } catch (IOException e) {
            System.out.println(e);
        }
    }


}
