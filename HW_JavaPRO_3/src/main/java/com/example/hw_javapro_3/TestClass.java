package com.example.hw_javapro_3;

class TestClass {
    @Test(a = 2, b = 5)
    public static void test(int a, int b) {
        int sum = a + b;
        System.out.println("Реализация домашнего задания 3.1");
        System.out.println("Сложение параметров аннотации Test равно: " + sum);
    }
}