package com.example.hw_javapro_3;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(value = {ElementType.TYPE, ElementType.TYPE_USE, ElementType.METHOD})
@Retention(value = RetentionPolicy.RUNTIME)
public @interface SaveTo {
    String path() default "file.txt";
}
