module com.example.hw_javapro_3 {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.desktop;


    opens com.example.hw_javapro_3 to javafx.fxml;
    exports com.example.hw_javapro_3;
}