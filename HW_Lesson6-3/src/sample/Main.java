package sample;

public class Main {

	public static void main(String[] args) {
		System.out.println("Вывод на экран 10 строк со значением числа Пи");
		
		for(int i = 2; i <= 11; i++) {
		    String text = String.format("%." + i + "f", Math.PI);
		    System.out.println(text);
		}
	}
}
