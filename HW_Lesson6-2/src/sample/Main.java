package sample;

import java.util.Arrays;
import java.util.Scanner;


public class Main {
	
	public static void main(String[] args) {
		
		System.out.println("Введите строку из слов, разделённых пробелами: ");
		Scanner sc = new Scanner(System.in);
		String text = sc.nextLine();
		
		String[] arrText = text.split("[ ]");

		String word = "";
		
		for(int i = 0; i < arrText.length; i++) {
			if(arrText[i].length() > word.length()) {
				word = arrText[i];
			}
		}
		System.out.println("Cамое длинное слово в веденной строке: " + word);
	}
}
