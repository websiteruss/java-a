package sample;

import java.awt.Desktop;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.Map;

public class NetworkService {

	public static Map<String, List<String>> getHeader(String sepc) throws IOException {
		URL url = new URL(sepc);
		URLConnection connection = url.openConnection();
		return connection.getHeaderFields();
	}

	public static String getStringFromURL(String spec, String code) throws IOException {
		URL url = new URL(spec);
		URLConnection connection = url.openConnection();
		String text = "";
		try (BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream(), code))) {
			for (;;) {
				String temp = br.readLine();
				if (temp == null) {
					break;
				}
				text += temp + System.lineSeparator();
			}
			return text;
		}

	}

	public static void downloadFileFromURL(String str) throws IOException {

		File directory = new File("DirectoryUrl");
		directory.mkdirs();
		File file = new File(directory, "url.txt");

		String[] urlSplit = str.split(" ");
		try (PrintWriter pw = new PrintWriter(file)) {
			for (int i = 0; i < urlSplit.length; i++) {
				if (urlSplit[i].startsWith("href=\"http")) {
					System.out.println(urlSplit[i].substring(6, urlSplit[i].lastIndexOf("\"")));
					pw.println(urlSplit[i].substring(6, urlSplit[i].lastIndexOf("\"")));
				}
			}
		}
	}

	public static void openDirectory() throws IOException{
		File file = new File("DirectoryUrl");

		Desktop desktop = null;

		if (Desktop.isDesktopSupported()) {
			desktop = Desktop.getDesktop();
		}
		try {
			desktop.open(file);
		} catch (IOException e) {
			System.out.println(e);
		}
	}
}