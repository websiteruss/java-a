
package sample;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public class Main {

	public static void main(String[] args) {
		String spec = "https://dou.ua";

		try {
			String htmlText = NetworkService.getStringFromURL(spec, "UTF-8");
			NetworkService.downloadFileFromURL(htmlText);
			NetworkService.openDirectory();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
