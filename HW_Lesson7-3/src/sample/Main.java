package sample;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Random rn = new Random();
		System.out.println("Ведите размер массива целых чисел: ");
		Scanner sc = new Scanner(System.in);
		int num = sc.nextInt();
		int[] array = new int[num];
		for(int i = 0; i < array.length; i++) {
			array[i] = rn.nextInt(-10, 50);
		}
		System.out.println(Arrays.toString(array));
		
		System.out.println("Ведите искомый элемент: ");
		int x = sc.nextInt();

		System.out.println(linearSearch(x, array));

	}

	public static int linearSearch(int x, int[] array) {
		for(int i = 0; i < array.length; i++) {
			if (array[i] == x) {
				return i;
			}
		}	
		return -1;
	}
}

