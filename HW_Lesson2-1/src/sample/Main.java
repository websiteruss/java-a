package sample;

public class Main {

	public static void main(String[] args) {
		
		/*Площадь треугольника по формуле Герона равна 
		корню из произведения разностей полупериметра 
		треугольника (pP) и каждой из его сторон (a, b, c): */
		double sideA = 0.3;
		double sideB = 0.4;
		double sideC = 0.5;
		
		// Полупериметр
		double pP = (sideA + sideB + sideC) / 2;
		
		// Площадь треугольника
		double s = Math.sqrt(pP * (pP - sideA) * (pP - sideB) * (pP - sideC) );
		
		System.out.println(s); //0.059999999999999984
	}
}
