package sample;

import java.util.Arrays;

public class Main {

	public static void main(String[] args) {
		int[] array = new int[] {0, 5, 2, 4, 7, 1, 3, 19};
		int sum = 0;
		for (int element: array) {
			if(element % 2 != 0) {
				sum += 1;
			}
		}
			System.out.println(sum);
	}
}
