package sample;

import java.util.Scanner;


public class Main {
	
	public static void main(String[] args) {
		
		System.out.println("Please, input text: ");
		Scanner sc = new Scanner(System.in);
		String text = sc.nextLine();
		
		char[] arrText = text.toCharArray();

		int sumB = 0;
		for(char ch: arrText) {
			if(ch == 'b') {
				sumB += 1;
			}
		}
		System.out.println("Number of characters «b»: " + sumB);
	}
}