package sample;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		Triangle triangle1 = new Triangle();
		
		triangle1.setSideA(0.3);
		triangle1.setSideB(0.4);
		triangle1.setSideC(0.5);
 
		System.out.println("Площадь треугольника triangle1: " + triangle1.calculateTheArea());	
		
		Triangle triangle2 = new Triangle(0.5, 0.6, 0.7);	

		System.out.println("Площадь треугольника triangle2: " + triangle2.calculateTheArea());	
		
		System.out.println("Введите стороны(A, B, C) треугольника: ");
		System.out.println("Сторона A: ");
		double sideA = sc.nextDouble();
		System.out.println("Сторона B: ");
		double sideB = sc.nextDouble();
		System.out.println("Сторона C: ");
		double sideC = sc.nextDouble();
		
		Triangle triangle3 = new Triangle(sideA, sideB, sideC);

		System.out.println("Площадь треугольника: " + triangle3.calculateTheArea());
	}

}
